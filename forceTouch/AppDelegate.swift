//
//  AppDelegate.swift
//  forceTouch
//
//  Created by Li, Ruilun (ADE-ANS) on 2/20/18.
//  Copyright © 2018 sample. All rights reserved.
//

import UIKit

enum Shortcut: String {
  case openApp = "first"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    var isLaunchedFromQuickAction = false
    if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
      isLaunchedFromQuickAction = true
      handleQuickAction(shortcutItem: shortcutItem)
    } else {
      self.window?.backgroundColor = UIColor.white
    }
    return !isLaunchedFromQuickAction
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    
  }

  func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
    completionHandler(handleQuickAction(shortcutItem: shortcutItem))
  }

  func handleQuickAction(shortcutItem: UIApplicationShortcutItem) -> Bool {
    var handled = false
    let type = shortcutItem.type.components(separatedBy: ".").last!
    if let shortcutType = Shortcut.init(rawValue: type) {
      switch shortcutType {
      case .openApp:
        self.window?.backgroundColor = UIColor.blue
        handled = true
      }
    }
    return handled
  }
}

