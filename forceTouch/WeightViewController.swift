//
//  ViewController.swift
//  forceTouch
//
//  Created by Li, Ruilun (ADE-ANS) on 2/20/18.
//  Copyright © 2018 sample. All rights reserved.
//

import UIKit

class WeightViewController: UIViewController {
  @IBOutlet weak var forceLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  //When touches moved, it capture the degree of the touch and calculate to estimate weight
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let touch = touches.first {
      if #available(iOS 9.0, *) {
        if traitCollection.forceTouchCapability == UIForceTouchCapability.available {
          if touch.force >= touch.maximumPossibleForce {
            forceLabel.text = "385+ grams"
          } else {
            let force = touch.force/touch.maximumPossibleForce
            forceLabel.text = "\(385 * force) grams"
          }
        }
      }
    }
  }
  
  //Restore value to zero when touches ended
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    forceLabel.text = "0 gram"
  }
}

