//
//  PeekAndPopViewController.swift
//  forceTouch
//
//  Created by Li, Ruilun (ADE-ANS) on 2/20/18.
//  Copyright © 2018 sample. All rights reserved.
//

import UIKit

class PeekAndPopViewController: UIViewController {
  
  @IBOutlet weak var peekAndPopView: UIView!
  @IBOutlet weak var previewButton: UIButton!
  
 fileprivate var previewVC: PreviewViewController {
    guard let vc = storyboard?.instantiateViewController(withIdentifier: "PreviewVC") as? PreviewViewController else {
      fatalError("failed to instantiate storyboard scene: 'PreviewVC'")
    }
    return vc
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupPreview()
  }
  
  //Setup peek and pop if force touch is available
  fileprivate func setupPreview() {
    if traitCollection.forceTouchCapability == .available {
      registerForPreviewing(with: self, sourceView: previewButton)
    }
    previewButton.addTarget(self, action: #selector(goToPreview), for: .touchUpInside)
  }
  
  //Go to Preview View Controller
  @objc func goToPreview() {
    if let nc = self.navigationController {
      nc.pushViewController(previewVC, animated: true)
    }
  }
  
}

extension PeekAndPopViewController: UIViewControllerPreviewingDelegate {
  public func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
    return previewVC
  }
  
  public func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
    if let nc = self.navigationController {
      nc.pushViewController(previewVC, animated: true)
    }
      
  }
}
